angular

    .module("navigationBar", [])

    .controller('navbarController', ['$http', function ($http) {

        var vm = this;

        $http.get('linkconfig.json')
            .then(
            function (response) {

                // success 
                vm.data = response.data;
            },
            function (response) {

                // failure call back
                console.log(response);
            });
    }])

    .directive("navbar", function () {

        return {
			/*
                E for Element name
                A for Attribute
                C for Class
                M for Comment
            */
            scope: {
                datasource: '=',
                add: '&',
            },
            restrict: "E",
            templateUrl: "navigationbar.html",
            controller: 'navbarController',
            controllerAs: 'vm',
        }
    });